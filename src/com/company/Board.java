package com.company;

public class Board {
    public Tile[][] tab;


    public Board(){
        setBoard();
    }

    public void setBoard(){
        tab = new Tile[8][8];
        //empty tiles
        for(int i = 2; i < 6; i++){
            for(int j = 0; j < 8; j++){
                tab[i][j] = new Tile(j, i, null);
            }
        }
        //pawns
        for(int i = 0; i < 8; i++){
            tab[1][i] = new Tile(i, 1, new Pawn(false));
            tab[6][i] = new Tile(i, 6, new Pawn(true));
        }
        //Rooks
        tab[0][0] = new Tile(0,0, new Rook(false));
        tab[0][7] = new Tile(7,0, new Rook(false));
        tab[7][0] = new Tile(0,7, new Rook(true));
        tab[7][7] = new Tile(7,7, new Rook(true));

        //Knights
        tab[0][1] = new Tile(1, 0, new Knight(false));
        tab[0][6] = new Tile(6, 0, new Knight(false));
        tab[7][1] = new Tile(1, 7, new Knight(true));
        tab[7][6] = new Tile(6, 7, new Knight(true));

        //Bishops
        tab[0][2] = new Tile(2, 0, new Bishop(false));
        tab[0][5] = new Tile(5, 0, new Bishop(false));
        tab[7][2] = new Tile(2, 7, new Bishop(true));
        tab[7][5] = new Tile(5, 7, new Bishop(true));

        //Kings
        tab[0][4] = new Tile(4, 0, new King(false));
        tab[7][4] = new Tile(4, 7, new King(true));

        //Queens
        tab[0][3] = new Tile(3, 0, new Queen(false));
        tab[7][3] = new Tile(3, 7, new Queen(true));
    }

    public void printBoard(){
        System.out.println("   0   1   2   3   4   5   6   7");
        for(int i = 0; i < 8; i++){
            System.out.print(i + ": ");
            for(int j = 0; j < 8; j++){
                if(tab[i][j].getPiece() != null){
                    if(tab[i][j].getPiece().isWhile()){
                        System.out.print(tab[i][j].getPiece().getName() + "w| ");
                    }
                    else{
                        System.out.print(tab[i][j].getPiece().getName() + "b| ");
                    }
                }
                else{
                    System.out.print("  | ");
                }
            }
            System.out.println();
            System.out.println("   _______________________________");

        }
    }
}
