package com.company;


import java.util.Scanner;

public class Main {
    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        Board board = new Board();
        board.setBoard();
        //board.printBoard();

        board.tab[2][3].setPiece(board.tab[1][3].getPiece());
        board.tab[1][3].setPiece(null);

        board.tab[2][4].setPiece(board.tab[6][0].getPiece());
        board.tab[6][0].setPiece(null);

        board.tab[3][5].setPiece(board.tab[0][3].getPiece());
        board.tab[0][3].setPiece(null);

        System.out.println();

        board.printBoard();

        if(board.tab[7][0].getPiece().canMove(board, board.tab[7][0], board.tab[1][0])){
            System.out.println("ok");
        }
        else{
            System.out.println("not ok");
        }


    }
}
