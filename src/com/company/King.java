package com.company;

public class King extends Piece {

    private boolean checked;

    public King(boolean is_white) {
        super(is_white);
        this.setName("k");
    }

    public boolean isChecked(){
        return checked;
    }
    public void setChecked(){}

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public boolean canMove(Board tab, Tile start, Tile end) {
        if(end.getPiece() != null){
            if(end.getPiece().isWhile() == this.isWhile()){
                System.out.println("can't move on your piece");
                return false;
            }
        }

        int x = end.getX() - start.getX();
        int y = end.getY() - start.getY();

        if(Math.abs(x) == 1 || Math.abs(y) == 1){
            //valid move for king
        }

        return false;
    }
}
