package com.company;

public class Rook extends Piece{

    public Rook(boolean is_white) {
        super(is_white);
        this.setName("R");
    }

    @Override
    public boolean canMove(Board tab, Tile start, Tile end) {
        if(end.getPiece() != null){
            if(end.getPiece().isWhile() == this.isWhile()){
                System.out.println("can't move on your piece");
                return false;
            }
        }

        int x = end.getX() - start.getX();
        int y = end.getY() - start.getY();

        if(x == 0 || y == 0) {
            int dir = 3;
            int distance = Math.max(Math.abs(x), Math.abs(y));

            if (x > 0) {
                dir = 0;
            } else if (y > 0) {
                dir = 1;
            } else if (x < 0) {
                dir = 2;
            }

            int xCur = start.getX(), yCur = start.getY();
            for (int i = 1; i < distance; i++) {
                if (dir == 0) {
                    xCur = start.getX() + i;
                } else if (dir == 1) {
                    yCur = start.getY() + i;
                } else if (dir == 2) {
                    xCur = start.getX() - i;
                } else {
                    yCur = start.getY() - i;
                }

                if (tab.tab[yCur][xCur].getPiece() != null) {
                    return false;
                }
            }

            return true;
        }

        System.out.println("Rook doesn't move like that");
        return false;
    }
}
