package com.company;

public class Pawn extends Piece {

    public Pawn(boolean is_white) {
        super(is_white);
        this.setName("P");
    }


    @Override
    public boolean canMove(Board board, Tile start, Tile end) {
        if(end.getPiece() != null){
            if(end.getPiece().isWhile() == this.isWhile()){
                System.out.println("can't move on your piece");
                return false;
            }
        }

        int x = end.getX() - start.getX();
        int y = end.getY() - start.getY();


        if(this.isWhile()){
            //straight 1 or 2 tiles
            if(x == 0){
                if(board.tab[start.getY() - 1][start.getX()].getPiece() == null){
                    //1 tile
                    if(y == -1){
                        System.out.println("1 tile");
                        return true;
                    }
                    //2 tiles
                    else if(start.getY() == 6 && y == -2 && end.getPiece() == null){
                        System.out.println("2 tiles");
                        return true;
                    }
                }
            }
            else if(Math.abs(x) == 1 && y == -1 && end.getPiece() != null){
                System.out.println("Kill");
                return true;
            }
        }
        else{
            if(x == 0){
                if(board.tab[start.getY() + 1][start.getX()].getPiece() == null){
                    if(y == 1){
                        System.out.println("1 tile");
                        return true;
                    }
                    else if(start.getY() == 1 && y == 2 && end.getPiece() == null){
                        System.out.println("2 tiles");
                        return true;
                    }
                }
            }
            else if(Math.abs(x) == 1 && y == 1 && end.getPiece() != null){
                System.out.println("Kill");
                return true;
            }
        }

        return false;

    }
}
