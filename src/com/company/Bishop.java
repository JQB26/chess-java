package com.company;

public class Bishop extends Piece {

    public Bishop(boolean is_white) {
        super(is_white);
        this.setName("B");
    }

    @Override
    public boolean canMove(Board tab, Tile start, Tile end) {
        if(end.getPiece() != null){
            if(end.getPiece().isWhile() == this.isWhile()){
                System.out.println("can't move on your piece");
                return false;
            }
        }

        int x = end.getX() - start.getX();
        int y = end.getY() - start.getY();

        if(Math.abs(x) - Math.abs(y) == 0){
            int distance = Math.abs(x);
            int dir = 3;
            if(x > 0 && y > 0){
                dir = 0;
            }
            else if(x < 0 && y > 0){
                dir = 1;
            }
            else if(x < 0 && y < 0){
                dir = 2;
            }

            for(int i = 1; i < distance; i++){
                int xCur, yCur;
                if(dir == 0){
                    xCur = start.getX() + i;
                    yCur = start.getY() + i;
                }
                else if(dir == 1){
                    xCur = start.getX() - i;
                    yCur = start.getY() + i;
                }
                else if(dir == 2){
                    xCur = start.getX() - i;
                    yCur = start.getY() - i;
                }
                else{
                    xCur = start.getX() - i;
                    yCur = start.getY() - i;
                }

                if(tab.tab[yCur][xCur].getPiece() != null){
                    return false;
                }
            }
            return true;
        }

        System.out.println("Bishop doesn't move like that");
        return false;
    }
}
