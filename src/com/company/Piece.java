package com.company;

public abstract class Piece {
    private boolean is_alive = true;
    private boolean is_white = true;
    private boolean is_pinned = false;
    private String name = "";

    public Piece(boolean is_white){
        this.is_white = is_white;
    }

    public boolean isWhile(){
        return is_white;
    }
    public void setBlack(){
        is_white = false;
    }
    public boolean isAlive(){
        return is_alive;
    }
    public void setKilled(){
        is_alive = false;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setPinned(){
        is_pinned = true;
    }
    public void setUnpinned(){
        is_pinned = false;
    }
    public boolean isPinned(){
        return is_pinned;
    }

    public abstract boolean canMove(Board tab, Tile start, Tile end);

}
